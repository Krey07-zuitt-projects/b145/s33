const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes')
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://krey07:DBuser7@wdco28-course-booking.9nho6.mongodb.net/session33?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));


app.use(express.json());
app.use(express.urlencoded({extended: true}));



app.use('/tasks', taskRoutes)

app.listen(port, () => console.log(`My server is running successfully running at port ${port}`))