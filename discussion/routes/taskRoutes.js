const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/createTask", (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


router.delete("/deleteTask/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

router.put("/updateTask/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


// ACTIVITY

// RETRIEVE SPECIFIC TASK

router.get("/:id", (req, res) => {
	taskControllers.findTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Change status

router.put("/:id/completed",(req,res) => {
	taskControllers.completedTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
})




module.exports = router;