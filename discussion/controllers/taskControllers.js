const Task = require('../models/taskSchema');

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
};

// Create a task
module.exports.createTask = (reqBody) => {

	let newTask = new Task ({
		name: reqBody.name
	});

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}


module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}

		result.name = reqBody.name
			return result.save().then((updatedTask, savedErr) => {
				if(savedErr){
					console.log(savedErr)
					return false
				} else {
					return updatedTask
				}
			})
	})
}

// ACTIVITY

// RETRIEVE SPECIFIC TASK

module.exports.findTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}

// Change status

module.exports.completedTask = (taskId,reqBody) => {
	return Task.findById(taskId).then((result,err) => {
		if(err){
			console.log(err)
			return false
		} 
		result.status = reqBody.status
		return result.save().then((completedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return completedTask
			}
		})
	})
}
